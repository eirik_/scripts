import pygame.midi
pygame.midi.init()

for x in range(10):
    if(pygame.midi.get_device_info(x) != None and pygame.midi.get_device_info(x)[2] == 1):
        print("Input device nr: ", x)
        print(pygame.midi.get_device_info(x))


device_id_input = input("\nSelect your input device nr: ")
device_id_input = int(device_id_input)
print("")

for x in range(10):
    if(pygame.midi.get_device_info(x) != None and pygame.midi.get_device_info(x)[3] == 1):
        print("Output nr: ", x)
        print(pygame.midi.get_device_info(x))


device_id_output = input("\nSelect your output nr: ")
device_id_output = int(device_id_output)
print("")

note_range_from_input = [0, 53]

inport = pygame.midi.Input(device_id_input)
outport = pygame.midi.Output(device_id_output)

buffer_size = 1024

print("\nRunning...")

while True:
    read_buffer = pygame.midi.Input.read(inport, buffer_size)
    for x in read_buffer:
        if(x[0][1] >= note_range_from_input[0] and x[0][1] <= note_range_from_input[1]):
            pygame.midi.Output.note_on(outport, x[0][1], x[0][2], 0)
