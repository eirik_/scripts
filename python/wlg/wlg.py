'''
A command line tool for creating wordlists
'''

import argparse
import itertools
import time

t0 = time.time()

ap = argparse.ArgumentParser()
ap.add_argument("start", type=int, help="Start length")
ap.add_argument("stop", type=int, help="Stop length")
ap.add_argument("--characters", type=str,
                default="0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!\"#$%&'()*+,-./:; <=>?@[\]^_`{|}~",
                help="Character set to use. Optional. Default is 0-9, a-z, A-Z, 32 symbols and whitespace")
ap.add_argument("--output-file", "-o", required=False, type=str, default="wordlist.txt",
                help="Specify output file. Default is wordlist.txt.")
args = ap.parse_args()


data = []
wc = 0


def writeToFile():
    global wc
    try:
        with open(args.output_file, "a") as f:
            f.write("\n".join(data))
            wc += len(data)
            data.clear()

    except OSError:
        print("Problem opening or writing to file")


for i in range(args.start, args.stop+1):
    for j in itertools.product(args.characters, repeat=i):
        data.append("".join(j))
        if len(data) >= 10000000:
            writeToFile()

writeToFile()

print("{} words, written to {}".format(wc, args.output_file))
print("Duration {}".format(time.time() - t0))
