'''
Command line tool that removes duplicates in line separated password lists
'''

import argparse

ap = argparse.ArgumentParser()
ap.add_argument("input", type=str)
ap.add_argument("output", type=str)
args = ap.parse_args()

try:
    with open(args.input, "r") as f:
        lines = f.readlines()

except OSError:
    print("Problem with opening or reading file")

line_set = set()

for line in lines:
    line_set.add(line)

try:
    with open(args.output, "w") as f:
        for w in line_set:
            f.write(w)

except OSError:
    print("Problem opening or writing to file")
