"""Script that takes an Excel file and splits into new files based on values in a column.
Data in new files can be distributed into sheets"""

import pandas
import random
import argparse

ap = argparse.ArgumentParser()

ap.add_argument("input_file", type=str, help="File to input")
ap.add_argument("sheet", type=str, help="Sheet to work on")
ap.add_argument("sortingColumn", type=str, help="Column to sort by")
ap.add_argument("--rowsPerSheet", type=int, help="Number of rows per sheet in new files. Disabled by default",
                default=0, required=False)
ap.add_argument("--randomize", type=bool, help="Randomize rows", default=False, required=False)
args = ap.parse_args()

sheet = pandas.read_excel(args.input_file, sheet_name=args.sheet)

data = {}

#Makes a list in dict data for each unique value in sorting column
for x in sheet.get(args.sortingColumn):
    data['%s' % x] = []

#Place each row in list based on value in sorting column
for index, row in sheet.iterrows():
    data[sheet.at[index, args.sortingColumn]].append(row)

for k in data:
    if args.randomize == True:
        random.shuffle(data[k])

    #Makes a new DataFrame named the same as k
    globals()['%s' %k] = pandas.DataFrame()

    writer = pandas.ExcelWriter('out/%s.xlsx' %k, engine='xlsxwriter')

    # Puts rows in a DataFrame based on value in sorting column
    count = 0
    sheetNumber = 1

    #Places rows in DataFrame
    for val in data[k]:
        globals()['%s' %k] = globals()['%s' %k].append(val)
        count += 1
        # Writes each DataFrame to a file
        globals()['%s' % k].to_excel(writer, sheet_name='Sheet %i' % sheetNumber)
        if count == args.rowsPerSheet:
            # Clears the DataFrame
            globals()['%s' % k].drop(globals()['%s' % k].index, inplace=True)
            #Update counters
            sheetNumber += 1
            count = 0
