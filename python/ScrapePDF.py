import requests
from bs4 import BeautifulSoup
import argparse

ap = argparse.ArgumentParser()

ap.add_argument("url", type=str, help="URL to scrape")
ap.add_argument("-l", action="store_true", help="Writes file with PDF URL's found")

args = ap.parse_args()

def scrape_pdf(url):
    site = requests.get(url)
    site.raise_for_status()
    soup = BeautifulSoup(site.text, "html.parser")

    for x in soup.find_all("a"):
        if ".pdf" in x.get("href"):
            r_link = requests.get(x.get("href"))
            try:
                with open(str(x.get("href")).split("/")[-1], "wb") as file:
                    for chunk in r_link.iter_content(100000):
                        file.write(chunk)
            except OSError:
                print("Problem opening or writing to file")


def scrape_pdf_list(url):
    site = requests.get(url)
    site.raise_for_status()
    soup = BeautifulSoup(site.text, "html.parser")

    try:
        with open(url.split("/")[2]+"_pdf_links.txt", "w") as file:
            file.write("PDF links from " + url + "\n")
            for x in soup.find_all("a"):
                if ".pdf" in x.get("href"):
                    file.write(x.get("href")+"\n")
    except OSerror:
        print("Problem opening or writing to file")


if args.l == True:
    scrape_pdf_list(args.url)
else:
    scrape_pdf(args.url)
