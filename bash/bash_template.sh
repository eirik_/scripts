#!/usr/bin/env bash

PROGNAME=$(basename $0)
LOGFILE=

write_log(){
    #Write to log
    while read text
    do
        LOGTIME=$(date "+%Y-%m-%d %H:%M:%S")
        # If log file is not defined, just echo the output
        if [ "$LOGFILE" == "" ]
        then
            echo $LOGTIME": $text"
        else
            LOG=$LOGFILE.$(date +%Y-%m)
            touch $LOG
            if [ ! -f $LOG ]
            then
                echo "ERROR!! Cannot create log file $LOG. Exiting."
                exit 1
            fi
            echo "$LOGTIME $PROGNAME $text" >> $LOG
        fi
    done
}

error_exit(){
    #Error function
    echo "${PROGNAME}: ${1:-"Unknown error"}, aborting" 1>&2
    exit 1
}


echo "Starting..." | write_log

#CODE | write_log

echo "Finishing" | write_log
