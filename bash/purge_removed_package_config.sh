#!/usr/bin/env bash

dpkg -l | grep ^rc | awk '{print $2}' | xargs dpkg -P