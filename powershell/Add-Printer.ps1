﻿$PrinterNameColor = ""
$PrinterNameBlack = ""
$DriverPath = ""
$DriverName = ""
$PrinterAddress = ""

Add-PrinterPort -Name $PrinterAddress -PrinterHostAddress $PrinterAddress
Add-PrinterDriver -Name $DriverName -InfPath $DriverPath

Add-Printer -Name $PrinterNameColor -DriverName $DriverName -PortName $PrinterAddress