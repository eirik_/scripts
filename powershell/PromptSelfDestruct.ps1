$CurrentScriptFullPathName = $MyInvocation.MyCommand.Definition
$CurrentScriptName = $MyInvocation.MyCommand.Name
clear
write ""
write "Self-Destruct option: "
$SelfDestructChoice = Read-Host "Do you wish for this script ($CurrentScriptName) to destroy itself upon completion? (Enter Yes or No)"
if ("$SelfDestructChoice" -eq "No")
{
  write ""
  write "Okay, this script will not Self-Destruct."
  write "Keeping: $CurrentScriptFullPathName"
  write ""
}
elseif ("$SelfDestructChoice" -eq "Yes")
{
  write ""
  write "You pushed the Self-Destruct button! This script will now attempt to delete itself..." 
  #write ""
  write "Deleting: $CurrentScriptFullPathName"
  Remove-Item $CurrentScriptFullPathName
  Start-Sleep 3
  write ""
  write "Validating removal...  Is $CurrentScriptName still present?"
  Start-Sleep 3
  Test-Path $CurrentScriptFullPathName
  write ""
  Start-Sleep 3
}
else
{
  write ""
  write "Invalid OverRide choice: "$SelfDestructChoice" is not valid.  Must be Yes or No. Exiting!"
  write ""
  exit
}
write "Complete! Exiting..."
Start-Sleep 3