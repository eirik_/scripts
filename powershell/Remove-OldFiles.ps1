﻿Start-Transcript "C:\DeleteLog $(get-date -Format dd-MM-yyyy).txt"

Get-ChildItem <#mappebane#> | Where-Object {$_.LastWriteTime -lt (get-date).AddDays(-30)} | Remove-Item -Recurse -Verbose -WhatIf

#husk å fjern -Whatif

Stop-Transcript