﻿Import-Module MSOnline

$365AdminCred = Get-Credential
Connect-MsolService -Credential $365AdminCred


Get-MsolUser | select userprincipalname, displayname, passwordneverexpires


Set-MsolUser -UserPrincipalName eksempel@eksempel.com -PasswordNeverExpires $true


Get-MsolUser | Set-MsolUser -PasswordNeverExpires $true
