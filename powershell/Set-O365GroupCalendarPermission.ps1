Import-Module MSOnline
Import-Module MSOnlineExtended

$UserCredential = Get-Credential
$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://outlook.office365.com/powershell-liveid/ -Credential $UserCredential -Authentication Basic -AllowRedirection

    Import-PSSession $Session

        $GroupMembers = Get-DistributionGroupMember Alle
        foreach ($GroupMember in $GroupMembers) { Set-MailboxFolderPermission -Identity (""+ $GroupMember.Guid+ ":\Kalender") -User Standard -AccessRights Reviewer}

        foreach ($GroupMember in $GroupMembers) { Set-MailboxFolderPermission -Identity (""+ $GroupMember.Guid+ ":\Calendar") -User Standard -AccessRights Reviewer}

    Remove-PSSession $Session