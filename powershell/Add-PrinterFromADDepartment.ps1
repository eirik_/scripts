﻿$password=Get-Content \\FolderPath\cred.txt | ConvertTo-SecureString -Key (86,48,2,48,9,62,17,85,5,235,184,1,9,8,4,74,5,154,23,87,42,56,84,69)
$admin=New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList "UserWithAdminAccess", $password

$sess=New-PSSession -ComputerName ADServer -Credential $admin

Invoke-Command -Session $sess -ScriptBlock {Import-Module activedirectory}
Invoke-Command -Session $sess -ScriptBlock {New-PSDrive -PSProvider activedirectory -Name PSDriveName -Root "AD:\PathToOU"}
Invoke-Command -Session $sess -ScriptBlock {Set-Location PSDriveName:}

$avdeling=Invoke-Command -Session $sess -ScriptBlock {Get-ItemProperty -Filter mailnickname=$using:env:USERNAME -Name department -Path *} | Select-Object department

$avdnr=$avdeling.department

Add-Printer -ConnectionName \\PrintServer\Printer-$avdnr