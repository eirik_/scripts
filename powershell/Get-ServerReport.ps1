﻿$CompName=$env:COMPUTERNAME  


$getos=Get-CimInstance -ClassName Win32_OperatingSystem |  
select Caption, ServicePackMajorVersion, LastBootUpTime  
 

$getcomp=Get-CimInstance -ClassName Win32_ComputerSystem  |  
select Manufacturer, Model, TotalPhysicalMemory, NumberOfProcessors, NumberOfLogicalProcessors | Format-Table

 
 
$getmem=Get-CimInstance -ClassName Win32_PhysicalMemory


 
$getdisk=Get-CimInstance -ClassName Win32_LogicalDisk -Filter "DriveType=3" | 
select DeviceId, VolumeName, Size, FreeSpace 
  
  
$getnic=Get-CimInstance -ClassName Win32_NetworkAdapterConfiguration -Filter "IPEnabled=$true" |  
 foreach { 
   select -InputObject $psitem -Property Description, DHCPEnabled, IPAddress, IPSubnet,  
   DefaultIPGateway, DNSServerSearchOrder, MACAddress, 
   @{Name="Name";Expression={Get-CimInstance -ClassName  Win32_NetworkAdapter -Filter "DeviceId=$($_.Index)" | select -ExpandProperty NetConnectionID }} 
   }


$getserv=Get-CimInstance -ClassName Win32_Service | select DisplayName, Name, StartMode, State, Status, StartName


Out-File -FilePath $HOME\$CompName.txt -InputObject $getos -Width 100

Out-File -FilePath $HOME\$CompName.txt -InputObject $getcomp -Append -Width 100

Out-File -FilePath $HOME\$CompName.txt -InputObject $getdisk -Append -Width 100

Out-File -FilePath $HOME\$CompName.txt -InputObject $getnic -Append -Width 100

Out-File -FilePath $HOME\$CompName.txt -InputObject $getserv -Append -Width 100